const express = require("express");
const app = express();
app.use(express.json());
const cors = require("cors");
app.use(
  cors({
    origin: "*"
  })
);

const port = 5000;

let uploadedImageUrl = {};

app.get("/", (req, res) => {
  res.send("Home route is here !!!");
});

app.get("/api/urls", (req, res) => {
  res.json(uploadedImageUrl);
});

app.post("/api/urls", (req, res) => {
  uploadedImageUrl = {
    id: 1,
    name: "new image",
    url: req.body.url
  };

  res.json(uploadedImageUrl);
});

app.put("/api/urls/update", (req, res) => {
  uploadedImageUrl = {
    id: 1,
    name: "updated image",
    url: req.body.url
  };

  res.json(uploadedImageUrl);
});

app.listen(port, () => {
  console.log("I'm listening on port " + port);
});
